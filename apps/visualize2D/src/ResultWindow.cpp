//
// Created by lennard on 6/10/20.
//

#include "ResultWindow.h"
#include "TriangleItem.h"
#include "Ray2DItem.h"
#include "AABox2DItem.h"

using gridbox::ResultWindow;

void ResultWindow::draw(const TriangleGrid& grid) {

  for(const Triangle2DByArray& triangle : grid.primitives())
    scene->addItem(new TriangleItem(triangle));

  fitScene();
}

void ResultWindow::draw(const Ray2DWithPrecomputation& ray) {
//  Ray2DItem* rayItem = new Ray2DItem(ray, _viewRect);
//  scene->addEllipse(rayItem->line().p1().x(), rayItem->line().p1().y(), 0,0, Qt::NoPen);
//  fitScene();
  scene->addItem(new Ray2DItem(ray, scene->sceneRect()));
}

void ResultWindow::draw(const TriangleRegion& region) {
  for(const Triangle2DByArray& triangle : region)
    scene->addItem(new TriangleItem(triangle, QPen(Qt::green, 7)));
}

void ResultWindow::draw(const TriangleRegions& regions) {
  for(const TriangleRegion& region : regions)
    draw(region);
}

void ResultWindow::draw(const AABox2DByMinMax& aab) {
  scene->addItem(new AABox2DItem(aab));
}

void ResultWindow::fitScene() {
  QRectF sceneRect = scene->itemsBoundingRect();
  sceneRect.setWidth(sceneRect.width()*2.5);
  sceneRect.setHeight(sceneRect.height()*2);
  view->fitInView(sceneRect, Qt::KeepAspectRatio);
}