//
// Created by lennard on 6/19/20.
//

#pragma once

#include <QtWidgets/QGraphicsRectItem>
#include <QPen>
#include <QPainter>
#include <geometry/implementations/d2/AABox2DByMinMax.h>

namespace gridbox {
  class AABox2DItem : public QGraphicsRectItem {
  public:
    using AABox2D = AABox2DByMinMax;

    AABox2DItem(const AABox2D& aab);
    AABox2DItem(const AABox2D& aab, const QPen& pen);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

  private:
    QPen _pen = QPen(Qt::magenta, 3);
  };
}