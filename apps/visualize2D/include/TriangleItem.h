//
// Created by lennard on 6/19/20.
//

#pragma once

#include <QtWidgets/QGraphicsPolygonItem>
#include <QPen>
#include <QPainter>
#include <geometry/implementations/d2/Triangle2DByArray.h>

namespace gridbox {
  class TriangleItem : public QGraphicsPolygonItem {
  public:
    using Triangle2D = Triangle2DByArray;
    using Point2D = Point2DByValarray;

    TriangleItem(const Triangle2D& triangle){
      QPolygonF poly;
      for(const Point2D& point : points(triangle))
        poly.append(QPointF(100.0*point[0], -100.0*point[1]));
      setPolygon(poly);
    }
    TriangleItem(const Triangle2D& triangle, const QPen& pen)
        : TriangleItem(triangle)
    {
      _pen = pen;
    }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
      painter->setPen(_pen);
      painter->drawPolygon(polygon());
    }

  private:
    QPen _pen = QPen(Qt::black, 3);
  };
}