//
// Created by lennard on 8/1/20.
//

#include "ConstructionBenchmark.h"

namespace gridbox {

  std::ostream& operator<<(std::ostream& o, const construction_bm_result& bmResult) {

    o << "construction result:" << '\n'
      << " constructionTime: " << bmResult.constructionTime << '\n'
      << " height: " << bmResult.height << '\n'
      << " fillRates: " << toString(bmResult.fillRates) << '\n'
      << " relativeIncrease: " << toString(bmResult.relativeIncrease) << '\n'
      << " absoluteIncrease: " << bmResult.absoluteIncrease << '\n'
      << " utilization: " << bmResult.utilization << '\n'
      << " memoryUsage: " << bytesToReadableStr(bmResult.memoryUsage) << '\n'
      ;
    return o;
  }

  void addMeansDistinctToFiles(const construction_bm_result& bmResult, const std::filesystem::path& dirPath) {
    std::filesystem::create_directories(dirPath);
    std::string id = std::to_string(bmResult.maxRegionSize);

    addDistinctResultToFile(id, std::to_string(bmResult.constructionTime.count()), dirPath / "constructionTime");
    addDistinctResultToFile(id, std::to_string(bmResult.height), dirPath / "height");
    for(int i=0; i<bmResult.fillRates.size(); i++)
      addDistinctResultToFile(std::to_string(i), std::to_string(bmResult.fillRates[i]), dirPath / ("fillRates" + id));
    for(int i=0; i<bmResult.relativeIncrease.size(); i++)
      addDistinctResultToFile(std::to_string(i), std::to_string(bmResult.relativeIncrease[i]), dirPath / ("relativeIncrease" + id));
    addDistinctResultToFile(id, std::to_string(bmResult.absoluteIncrease), dirPath / "absoluteIncrease");
    addDistinctResultToFile(id, std::to_string(bmResult.utilization.mean), dirPath / "utilization");
    addDistinctResultToFile(id, std::to_string(bmResult.memoryUsage), dirPath / "memoryUsage");
  }
}