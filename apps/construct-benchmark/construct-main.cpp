
#include "ConstructionBenchmark.h"
#include <algorithms/implementations/d3/OctreeTetraGridSearch.h>

INITIALIZE_EASYLOGGINGPP

int main(int argc, char** argv) {
  using namespace gridbox;

  el::Configurations defaultConf;
  defaultConf.setGlobally(el::ConfigurationType::Format, "%level: %msg");
  defaultConf.set(el::Level::Trace, el::ConfigurationType::Enabled, "false");
  el::Loggers::reconfigureLogger("default", defaultConf);

  if(argc < 5){
    LOG(ERROR) << "not enough arguments";
    LOG(INFO) << "usage: ./construct-benchmark <GridFile> <out ResultFile> <out GridSearchFile> <MaxRegionSize>";
    return -1;
  }

  std::string gridFile = argv[1];
  std::string outResultFile = argv[2];
  std::string gridSearchFile = argv[3];
  u_long maxRegionSize = std::stoul(argv[4]);

  TetraGrid grid;
  loadFromFile(grid, gridFile);

  OctreeTetraGridSearch ogs;
  auto bmResult = constructionBenchmark(ogs, grid, maxRegionSize);
  LOG(INFO) << bmResult;

  saveToFile(ogs, gridSearchFile);
  addMeansDistinctToFiles(bmResult, outResultFile);

  return 0;
}
