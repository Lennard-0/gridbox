//
// Created by lennard on 10/14/20.
//
#pragma once

#include <easylogging++.h>

#include <GL/glew.h> // should be included before GLFW
#include <GLFW/glfw3.h>

namespace gridbox {
  static void glfw_error_callback(int error, const char* description)
  {
    LOG(ERROR) << "Error: " << description << '\n';
  }

  class OpenglWindow {
  public:
    OpenglWindow();

    void create();
    [[nodiscard]] uint width() const;
    [[nodiscard]] uint height() const;
    bool shouldClose();
    void finalizeFrame();
    void close();
  private:
    GLFWwindow* _window = nullptr;
    uint _width, _height;
  };
}