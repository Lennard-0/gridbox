//
// Created by lennard on 10/12/20.
//

#include <algorithms/implementations/d3/OctreeTetraGridSearch.h>
#include "ThreadRenderer.h"

#include "OpenglWindow.h"

INITIALIZE_EASYLOGGINGPP

int main(int argc, char** argv) {
  using namespace gridbox;

  el::Configurations defaultConf;
  defaultConf.setGlobally(el::ConfigurationType::Format, "%level: %msg");
  defaultConf.set(el::Level::Trace, el::ConfigurationType::Enabled, "false");
  el::Loggers::reconfigureLogger("default", defaultConf);

  if(argc < 2){
    LOG(ERROR) << "not enough arguments";
    LOG(INFO) << "usage: ./render3D <GridSearchFile>";
    return -1;
  }
  std::string gsFilePath = argv[1];

  OctreeTetraGridSearch octreeGS;
  loadFromFile(octreeGS, gsFilePath);

  OpenglWindow window;
  window.create();
  OpenglBuffer glBuffer(window.width(), window.height());
  glBuffer.create();

  auto rays = createRays(aabb(octreeGS), window.width(), window.height());
  LOG(DEBUG) << "first ray: " << rays[0];
  LOG(DEBUG) << "last ray: " << rays.rbegin()[0];

  ThreadRenderer tr;
  tr.setOutputBuffer(glBuffer);

  while(!window.shouldClose()) {
    tr.render(octreeGS, rays);
    glBuffer.drawToFrame();
    window.finalizeFrame();
  }

  window.close();
  return 0;
}