
#include "QueryBenchmark.h"
#include <algorithms/implementations/d3/OctreeTetraGridSearch.h>

INITIALIZE_EASYLOGGINGPP

int main(int argc, char** argv) {
  using namespace gridbox;

  el::Configurations defaultConf;
  defaultConf.setGlobally(el::ConfigurationType::Format, "%level: %msg");
  defaultConf.set(el::Level::Trace, el::ConfigurationType::Enabled, "false");
  el::Loggers::reconfigureLogger("default", defaultConf);

  if(argc < 4){
    LOG(ERROR) << "not enough arguments";
    LOG(INFO) << "usage: ./query-benchmark <GridSearchFile> <TestRaysFile> <out ResultsDir>";
    return -1;
  }
  std::string gsFilePath = argv[1];
  std::string testRaysFilePath = argv[2];
  std::string bmOutDir = argv[3];

  OctreeTetraGridSearch octreeGS;
  loadFromFile(octreeGS, gsFilePath);
  auto benchmarkResult = queryBenchmark(octreeGS, testRaysFilePath);
  LOG(INFO) << benchmarkResult;
  addMeansDistinctToFiles(benchmarkResult, bmOutDir);

  return 0;
}
