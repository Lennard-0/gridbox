#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

modprobe msr
#echo 0 > /proc/sys/kernel/nmi_watchdog
chown root cmake-build-debug/bin/query-bm
chmod 4755 cmake-build-debug/bin/query-bm
chown root cmake-build-release/bin/query-bm
chmod 4755 cmake-build-release/bin/query-bm
chown root cmake-build-relwithdebinfo/bin/query-bm
chmod 4755 cmake-build-relwithdebinfo/bin/query-bm