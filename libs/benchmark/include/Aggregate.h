//
// Created by lennard on 7/19/20.
//

#pragma once

#include <chrono>
#include <set>
#include <limits>
#include <cmath>

#include <core/Helper.h>

namespace gridbox {


  template<class T>
  double median(const std::multiset<T>& data)
  {
    if (data.empty())
      throw std::length_error("Cannot calculate median value for empty dataset");

    const size_t n = data.size();
    double median = 0;

    auto iter = data.cbegin();
    std::advance(iter, n / 2);

    if (n % 2 == 0) //even number of values
      median = double(*iter + *--iter) / 2;    // data[n/2 - 1] AND data[n/2]
    else
      median = *iter;

    return median;
  }

  std::chrono::nanoseconds median(const std::multiset<std::chrono::nanoseconds>& data);

  template<class T>
  T sum(const std::multiset<T>& data) {
    auto it = data.begin();
    T result = *it++;
    for(; it != data.end(); it++)
      result += *it;
    return result;
  }
  template<class T>
  double mean(const std::multiset<T>& data) {
    return (double) sum(data) / data.size();
  }
  std::chrono::nanoseconds mean(const std::multiset<std::chrono::nanoseconds>& data);

  template<class T>
  double sd(const std::multiset<T>& data, const double& mean) {
    double sumSquaredDeviations = 0;
    for(const T& el : data)
      sumSquaredDeviations += (el-mean) * (el-mean);
    double variance = sumSquaredDeviations / data.size();

    return std::sqrt(variance);
  }
  std::chrono::nanoseconds sd(const std::multiset<std::chrono::nanoseconds>& data, const std::chrono::nanoseconds& mean);

  struct benchmark {

    };
  template<typename T>
  concept benchmarkC = std::is_base_of_v<benchmark, T>;

  struct benchmark_time : public benchmark {
    std::chrono::nanoseconds min = std::chrono::nanoseconds::max();
    std::chrono::nanoseconds max = std::chrono::nanoseconds::min();
    std::chrono::nanoseconds mean;
    std::chrono::nanoseconds median;
    std::chrono::nanoseconds sd;
    std::multiset<std::chrono::nanoseconds> values;
  };

  struct benchmark_double : public benchmark {
    double min = std::numeric_limits<double>::infinity();
    double max = -std::numeric_limits<double>::infinity();
    double mean;
    double median;
    double sd;
    std::multiset<double> values;
  };

  struct benchmark_uint64 : public benchmark {
    uint64_t min = std::numeric_limits<uint64_t>::max();
    uint64_t max = 0;
    double mean;
    double median;
    double sd;
    std::multiset<uint64_t> values;
  };

  struct benchmark_bytes : public benchmark_uint64 {};
  template<typename T>
  concept benchmarkBytesC = benchmarkC<T> && std::is_base_of_v<benchmark_bytes, T>;

  template<typename benchmark, typename parameter>
  void addResult(benchmark& benchm, const parameter& para) {
    benchm.values.emplace(para);
    if(para < benchm.min)
      benchm.min = para;
    if(para > benchm.max)
      benchm.max = para;
    benchm.mean = mean(benchm.values);
    benchm.median = median(benchm.values);
    benchm.sd = sd(benchm.values, benchm.mean);
  }

  template<benchmarkC benchmark, typename parameter>
  inline benchmark& operator+=(benchmark& bm, const parameter& prm) {
    addResult(bm, prm);
    return bm;
  }

  template<benchmarkC benchmark>
  std::ostream& operator<<(std::ostream& o, const benchmark& bm) {
    o << "mean=" << bm.mean << " median=" << bm.median << " min=" << bm.min << " max=" << bm.max << " sd=" << bm.sd;
    return o;
  }
  template<benchmarkBytesC benchmark>
  std::ostream& operator<<(std::ostream& o, const benchmark& bm) {
    o << "mean=" << bytesToReadableStr(bm.mean) << " median=" << bytesToReadableStr(bm.median) << " min=" << bytesToReadableStr(bm.min) << " max=" << bytesToReadableStr(bm.max) << " sd=" << bytesToReadableStr(bm.sd);
    return o;
  }

  void addDistinctResultToFile(const std::string& id, const std::string& value, const std::filesystem::path& filePath);
}