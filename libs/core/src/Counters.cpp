//
// Created by lennard on 7/19/20.
//

#include "core/Counters.h"

namespace gridbox {

  std::map<std::string, std::map<std::pair<std::string,std::string>,unsigned int>>& operationCounter() {
    static std::map<std::string, std::map<std::pair<std::string,std::string>,unsigned int>> counter = {};
    return counter;
  }

  unsigned int& operationCount(const std::string& operationName, const std::string& cName1, const std::string& cName2) {
    auto& counter = operationCounter();
    auto cNamePair = std::make_pair(cName1, cName2);
    if( ! counter.contains(operationName))
      counter.emplace(operationName, std::map<std::pair<std::string,std::string>,unsigned int>{});
    if( ! counter.at(operationName).contains(cNamePair))
      counter.at(operationName).emplace(cNamePair, 0);
    return counter.at(operationName).at(cNamePair);
  }

  void incrementOperationCounter(const std::string& operationName, const std::string& cName1, const std::string& cName2) {
    for(auto const& cPair : permutationPairs(uniqueConceptNames(cName1), uniqueConceptNames(cName2)))
      operationCount(operationName, cPair.first, cPair.second)++;
  }

  void resetCounters() {
#pragma omp critical
    operationCounter() = {};
  }
}