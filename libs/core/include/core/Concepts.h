//
// Created by lennard on 7/19/20.
//

#pragma once

#include "Helper.h"

#include <map>

namespace gridbox {

  std::map<std::string, std::set<std::string>>& conceptBases();
  bool addRelation(const std::string& cName, const std::set<std::string>& bases);
  std::set<std::string> uniqueConceptNames(const std::string& cName);


  class CConcept{ //todo: remove global base if never used
  public:
    static constexpr auto Name = "Concept";
  };
  template<typename T>
  concept ConceptC = std::is_base_of_v<CConcept, typename T::Concept>;


  #define GET_MACRO(_1,_2,_3,_4,NAME,...) NAME
  #define DEFINE_CONCEPT(...) GET_MACRO(__VA_ARGS__, DEFINE_CONCEPT4, DEFINE_CONCEPT3, DEFINE_CONCEPT2, DEFINE_CONCEPT1)(__VA_ARGS__)

  #define DEFINE_CONCEPT1(CName) \
  class C##CName : virtual CConcept { public: static constexpr auto Name = #CName; }; \
  namespace NS##CName { \
    const bool relationAdded = addRelation(#CName, {}); \
  } \
  template<typename T> \
  concept CName##C = ConceptC<T> && std::is_base_of_v<C##CName, typename T::Concept>

  #define DEFINE_CONCEPT2(CName,BaseCName) \
  class C##CName : virtual C##BaseCName { public: static constexpr auto Name = #CName; }; \
  namespace NS##CName { \
    const bool relationAdded = addRelation(#CName, {#BaseCName}); \
  } \
  template<typename T> \
  concept CName##C = BaseCName##C<T> && std::is_base_of_v<C##CName, typename T::Concept>

  #define DEFINE_CONCEPT3(CName,BaseCName1,BaseCName2) \
  class C##CName : virtual C##BaseCName1, virtual C##BaseCName2 { public: static constexpr auto Name = #CName; }; \
  namespace NS##CName { \
    const bool relationAdded = addRelation(#CName, {#BaseCName1, #BaseCName2}); \
  } \
  template<typename T> \
  concept CName##C = BaseCName1##C<T> && BaseCName2##C<T> && std::is_base_of_v<C##CName, typename T::Concept>

  #define DEFINE_CONCEPT4(CName,BaseCName1,BaseCName2,BaseCName3) \
  class C##CName : virtual C##BaseCName1, virtual C##BaseCName2, virtual C##BaseCName3 { public: static constexpr auto Name = #CName; }; \
  namespace NS##CName { \
    const bool relationAdded = addRelation(#CName, {#BaseCName1, #BaseCName2, #BaseCName3}); \
  } \
  template<typename T> \
  concept CName##C = BaseCName1##C<T> && BaseCName2##C<T> && BaseCName3##C<T> && std::is_base_of_v<C##CName, typename T::Concept>

  // BaseC<T> is needed because the compiler must be able to prove that CNameC is more constrained than
  // BaseC. Otherwise template overloading does not work
  // https://stackoverflow.com/questions/61907826/what-are-the-rules-for-ordering-concept-constrained-functions-in-c20



}