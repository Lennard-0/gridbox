//
// Created by lennard on 7/19/20.
//

#pragma once

#include "Concepts.h"

namespace gridbox {

  std::map<std::string, std::map<std::pair<std::string,std::string>,unsigned int>>& operationCounter();
  unsigned int& operationCount(const std::string& operationName, const std::string& cName1, const std::string& cName2);

  void incrementOperationCounter(const std::string& operationName, const std::string& cName1, const std::string& cName2);
  void resetCounters();


  #ifdef NDEBUG
    #define INCREMENT_OP(OPERATION, TypeNameA, TypeNameB)
  #else
    #define INCREMENT_OP(OPERATION, TypeNameA, TypeNameB) _Pragma("omp critical") \
    incrementOperationCounter(#OPERATION, TypeNameA::Concept::Name, TypeNameB::Concept::Name)
  #endif

}