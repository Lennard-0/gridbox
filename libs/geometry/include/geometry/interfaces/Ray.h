//
// Created by lennard on 6/29/20.
//

#pragma once

#include "Point.h"

namespace gridbox {

  template<RayC Ray>
  inline typename Ray::Point origin(const Ray& ray);
  template<RayC Ray>
  inline typename Ray::Vector direction(const Ray& ray);
  template<RayC Ray>
  inline typename Ray::Vector mulInvDirection(const Ray& ray);
  template<RayC Ray>
  inline typename Ray::Point rayPoint(const Ray& ray, const fp_t& t);
  template<RayC Ray>
  inline std::vector<typename Ray::Point> rayPoints(const Ray& ray, const std::vector<fp_t>& lambdas);

  template<RayC Ray>
  std::ostream& operator<<(std::ostream& o, const Ray& ray);
}