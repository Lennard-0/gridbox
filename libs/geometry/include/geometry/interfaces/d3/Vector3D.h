//
// Created by lennard on 7/13/20.
//

#pragma once

#include "Concepts.h"

namespace gridbox {

  template<Vector3DC Vector3D>
  inline Vector3D crossProduct(const Vector3D& lhs, const Vector3D& rhs);
}