//
// Created by lennard on 7/13/20.
//

#pragma once

#include "Concepts.h"

namespace gridbox {

  template<AARectangleC AARectangle>
  inline typename AARectangle::Point minPoint(const AARectangle& aaRec);

  template<AARectangleC AARectangle>
  inline typename AARectangle::Point maxPoint(const AARectangle& aaRec);

  template<AARectangleC AARectangle>
  inline u_short fixedDimension(const AARectangle& aaRec);

  template<AARectangleC AARectangle>
  inline fp_t fixedValue(const AARectangle& aaRec);

  template<AARectangleC AARectangle>
  inline bool isFixedMin(const AARectangle& aaRec);

  template<AARectangleC AARectangle>
  inline auto intersectionLambdaPoint(const typename AARectangle::Ray& ray, const AARectangle& aaRec)
                                      -> std::optional<std::pair<fp_t, typename AARectangle::Point>>;

  template<AARectangleC AARectangle>
  inline auto intersectionLambdaPointInclusive(const typename AARectangle::Ray& ray, const AARectangle& aaRec)
                                               -> std::optional<std::pair<fp_t, typename AARectangle::Point>>;
}