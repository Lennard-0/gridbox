//
// Created by lennard on 6/29/20.
//

#pragma once

#include "Ray.h"
#include "PolyFacet.h"

namespace gridbox {

  template<ConvexPolytopeC ConvexPolytope>
  inline std::vector<typename ConvexPolytope::Facet> facets(const ConvexPolytope& convexPolytope);
  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionLambdasInOut(const typename ConvexPolytope::Ray&   ray,
                                       const ConvexPolytope&                 convexPolytope)
                                       -> std::optional<std::pair<fp_t, fp_t>>;
  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionPointsInOut(const typename ConvexPolytope::Ray&   ray,
                                      const ConvexPolytope&                 convexPolytope)
                        -> std::optional<std::pair<typename ConvexPolytope::Point, typename ConvexPolytope::Point>>;

  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionLambdaIn(const typename ConvexPolytope::Ray&   ray,
                                   const ConvexPolytope&                 convexPolytope) -> std::optional<fp_t>;
  template<ConvexPolytopeC ConvexPolytope>
  inline auto intersectionPointIn(const typename ConvexPolytope::Ray&   ray,
                                  const ConvexPolytope&                 convexPolytope)
                                  -> std::optional<typename ConvexPolytope::Point>;

  template<ConvexPolytopeC ConvexPolytope>
  inline bool contains(const ConvexPolytope& convexPoly, const typename ConvexPolytope::Point& point);
  template<ConvexPolytopeC ConvexPolytope>
  inline bool containsInInterior(const ConvexPolytope& convexPoly, const typename ConvexPolytope::Point& point);
}