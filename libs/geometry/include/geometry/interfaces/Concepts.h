//
// Created by lennard on 7/10/20.
//

#pragma once

#include <core/Concepts.h>
#include <core/Counters.h>
#include <sys/types.h>
#include <easylogging++.h>

namespace gridbox {
  using fp_t = float;
  const fp_t EPSILON = std::numeric_limits<fp_t>::epsilon();

  DEFINE_CONCEPT(Coordinates);
  DEFINE_CONCEPT(Vector, Coordinates);
  DEFINE_CONCEPT(Point, Coordinates);

  DEFINE_CONCEPT(Ray);
  DEFINE_CONCEPT(Plane);

  DEFINE_CONCEPT(PointSet);
  DEFINE_CONCEPT(LineSegment, PointSet);
  DEFINE_CONCEPT(PolyFacet, PointSet);
  DEFINE_CONCEPT(Triangle, PointSet);

  DEFINE_CONCEPT(ConvexPolytope, PointSet);
  DEFINE_CONCEPT(AABox, ConvexPolytope);


  #ifdef NDEBUG
    #define RAY_INTERSECT(TypeName)
  #else
    #define RAY_INTERSECT(TypeName) _Pragma("omp critical") \
    incrementOperationCounter("intersection", "Ray", TypeName::Concept::Name)
  #endif

  #define RAY_INTERSECT_COUNT(ConceptName) operationCount("intersection", "Ray", #ConceptName)

  template<ConceptC Concept>
  inline json toJson(const Concept& cncpt);

  template<ConceptC Concept>
  inline Concept fromJson(const json& cncptJSON);
}