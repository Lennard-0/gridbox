//
// Created by lennard on 6/29/20.
//

#pragma once

#include "ConvexPolytope.h"

namespace gridbox {

  template<AABoxC AABox>
  inline typename AABox::Point minPoint(const AABox& aab);
  template<AABoxC AABox>
  inline typename AABox::Point maxPoint(const AABox& aab);
  template<AABoxC AABox>
  inline typename AABox::Point midPoint(const AABox& aab);//todo: drawio
  template<AABoxC AABox>
  inline AABox aabb(const std::vector<typename AABox::Point>& points);//todo: entfernen
  template<AABoxC AABox>
  inline std::array<AABox, AABox::num_orthants> orthants(const AABox& aab);//todo: drawio
  // =quadrants in 2D and =octants in 3D
  // ordered lexicographically by dimensions

  template<AABoxC AABox>
  inline std::vector<typename AABox::Ray> createRays(const AABox& aaBox, const uint& width, const uint& height);
}