//
// Created by lennard on 6/26/20.
//

#pragma once

#include "Concepts.h"
#include <valarray>
#include <iostream>

namespace gridbox {

  template<CoordinatesC Coordinates>
  constexpr u_short dimensions(const Coordinates& coords);
  template<CoordinatesC Coordinates>
  inline std::valarray<fp_t> coordinates(const Coordinates& coords);
  template<CoordinatesC Coordinates>
  inline fp_t get(const Coordinates& coords, u_short i);

  template<CoordinatesC Coordinates>
  inline bool operator==(const Coordinates& lhs, const Coordinates& rhs);

  template<CoordinatesC Coordinates>
  std::ostream& operator<<(std::ostream& o, const Coordinates& coords);

  template<CoordinatesC Coordinates>
  struct lex_compare;
}