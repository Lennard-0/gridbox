//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/interfaces/Concepts.h"

namespace gridbox {

  DEFINE_CONCEPT(LineSegment2D, LineSegment);
  DEFINE_CONCEPT(LineSegmentPolyFacet, LineSegment2D, PolyFacet);
  DEFINE_CONCEPT(TrianglePoly, Triangle, ConvexPolytope);
  DEFINE_CONCEPT(AABox2D, AABox);
}