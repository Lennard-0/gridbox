//
// Created by lennard on 7/5/20.
//

#pragma once

#include "geometry/interfaces/PointSet.h"

namespace gridbox {

  DEFINE_CONCEPT(PointSetByArray, PointSet);

  template<PointC PointT, u_short N>
  class PointSetByArray {
  public:
    using Concept = CPointSetByArray;
    using Point = PointT;
    static const u_short NumPoints = N;
    using Vector = typename Point::Vector;

    PointSetByArray(const std::array<Point,NumPoints>& points)
    : _points(points)
    {}

    std::vector<Point> points() const {
      return {_points.begin(), _points.end()};
    }

  protected:
    std::array<Point, NumPoints> _points;
  };

  template<PointSetByArrayC PointSet>
  inline std::vector<typename PointSet::Point> points(const PointSet& pointSet) {
    return pointSet.points();
  }

  template<PointSetC PointSet>//todo: ist evtl implementierugngsspezifisch?
  inline json toJson(const PointSet& pointSet) {
    json result = {};
    json pointsJSON = json::array();
    for(auto const& point : points(pointSet))
      pointsJSON += toJson(point);
    result["points"] = pointsJSON;
    return result;
  }

}