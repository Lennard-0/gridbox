//
// Created by Lennard Ockenfels on 5/27/20.
//

#pragma once

#include "geometry/functions/Vector.h"
#include "Coordinates2DByValarray.h"

namespace gridbox {

  DEFINE_CONCEPT(Vector2DByValarray, Vector, Coordinates2DByValarray);

  class Vector2DByValarray : public Coordinates2DByValarray {
  public:
    using Concept = CVector2DByValarray;

    using Coordinates2DByValarray::Coordinates2DByValarray;

    Vector2DByValarray getTurned90DegreesCW() const {
      return {_coordinates[1], -_coordinates[0]};
    }
    Vector2DByValarray getTurned90DegreesCCW() const {
      return {-_coordinates[1], _coordinates[0]};
    }
  };


  template<>
  inline Vector2DByValarray random<Vector2DByValarray>() {
    fp_t t = 2.0 * M_PI * randomFloat01();
    return {std::cos(t), std::sin(t)};
  }
}