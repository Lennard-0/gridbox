//
// Created by lennard on 6/30/20.
//

#pragma once

#include "geometry/implementations/RayWithPrecomputation.h"
#include "Point2DByValarray.h"

namespace gridbox {
  using Ray2DWithPrecomputation = RayWithPrecomputation<Point2DByValarray>;
}