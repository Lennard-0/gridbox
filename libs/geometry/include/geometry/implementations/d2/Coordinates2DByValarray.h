//
// Created by lennard on 7/4/20.
//

#pragma once

#include "geometry/implementations/CoordinatesByValarray.h"

namespace gridbox {

  DEFINE_CONCEPT(Coordinates2DByValarray, CoordinatesByValarray);

  class Coordinates2DByValarray : public CoordinatesByValarray<2> {
  public:
    using Concept = CCoordinates2DByValarray;
    using CoordinatesByValarray<2>::CoordinatesByValarray;

    Coordinates2DByValarray(const fp_t& x, const fp_t& y)
    : CoordinatesByValarray({x,y})
    {}

    fp_t x() const {
      return _coordinates[0];
    }
    fp_t y() const {
      return _coordinates[1];
    }
  };
}