//
// Created by lennard on 8/1/20.
//

#pragma once

#include "geometry/implementations/SphereByPoint.h"
#include "Point2DByValarray.h"

namespace gridbox {

  DEFINE_CONCEPT(CircleByPoint, SphereByPoint);

  class CircleByPoint : public SphereByPoint<Point2DByValarray> {
  public:
    using Concept = CCircleByPoint;
    using SphereByPoint<Point2DByValarray>::SphereByPoint;
  };
}