//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/implementations/PointSetByArray.h"
#include "geometry/functions/ConvexPolytope.h"
#include "geometry/functions/Triangle.h"
#include "Point2DByValarray.h"
#include "LineSegment2DByArray.h"
#include "geometry/implementations/RayWithPrecomputation.h"
#include "LineSegmentPolyFacet.h"
#include "AABox2DByMinMax.h"

namespace gridbox {

  DEFINE_CONCEPT(Triangle2DByArray, TrianglePoly, PointSetByArray);

  class Triangle2DByArray : public PointSetByArray<Point2DByValarray, 3> {
  public:
    using Concept = CTriangle2DByArray;
    using LineSegment = LineSegment2DByArray;
    using Facet = LineSegmentPolyFacet;
    using AABB = AABox2DByMinMax;
    using Ray = Ray2DWithPrecomputation;

    using PointSetByArray<Point2DByValarray, 3>::PointSetByArray;

    Triangle2DByArray(const Point& a, const Point& b, const Point& c)
    : PointSetByArray<Point2DByValarray, 3>::PointSetByArray({a,b,c})
    {}
  };

  template<Triangle2DByArrayC TrianglePoly>
  inline std::vector<typename TrianglePoly::Facet> facets(const TrianglePoly& triangle) {
    auto result = lineSegments(triangle);
    return {result.begin(), result.end()};
  }
}