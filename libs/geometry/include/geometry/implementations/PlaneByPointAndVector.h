//
// Created by lennard on 7/4/20.
//

#pragma once

#include "geometry/functions/Plane.h"

namespace gridbox {

  DEFINE_CONCEPT(PlaneByPointAndVector, Plane);

  template<PointC PointT>
  class PlaneByPointAndVector {
  public:
    using Concept = CPlaneByPointAndVector;
    using Point = PointT;
    using Vector = typename Point::Vector;

    PlaneByPointAndVector(const Point& support, const Vector& normal)
    : _support(support)
    , _normal(normal)
    {}

    Point support() const {
      return _support;
    }
    Vector normal() const {
      return _normal;
    }

  protected:
    Point _support;
    Vector _normal;
  };


  template<PlaneByPointAndVectorC Plane>
  inline typename Plane::Point support(const Plane& plane) {
    return plane.support();
  }

  template<PlaneByPointAndVectorC Plane>
  inline typename Plane::Vector normal(const Plane& plane) {
    return plane.normal();
  }
}