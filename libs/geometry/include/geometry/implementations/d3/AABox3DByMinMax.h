//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/implementations/AABoxByMinMax.h"
#include "geometry/functions/d3/AABox3D.h"
#include "Point3DByValarray.h"
#include "AARectanglePolyFacet.h"
#include "Sphere3D.h"

namespace gridbox {

  DEFINE_CONCEPT(AABox3DByMinMax, AABox3D, AABoxByMinMax);

  class AABox3DByMinMax : public AABoxByMinMax<Point3DByValarray> {
  public:
    using Concept = CAABox3DByMinMax;
    using Point = Point3DByValarray;
    using Vector = Vector3DByValarray;
    using Facet = AARectanglePolyFacet;
    using Ray = Ray3DWithPrecomputation;
    using Sphere = Sphere3D;
    static const u_short NumPoints = 8;
    static const u_short NumFacets = 6;

    using AABoxByMinMax<Point3DByValarray>::AABoxByMinMax;
  };
}