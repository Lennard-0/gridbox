//
// Created by Lennard Ockenfels on 5/27/20.
//

#pragma once

#include "geometry/functions/d3/Vector3D.h"
#include "geometry/interfaces/d3/Concepts.h"
#include "Coordinates3DByValarray.h"

namespace gridbox {

  DEFINE_CONCEPT(Vector3DByValarray, Vector3D, Coordinates3DByValarray);

  class Vector3DByValarray : public Coordinates3DByValarray {
  public:
    using Concept = CVector3DByValarray;

    using Coordinates3DByValarray::Coordinates3DByValarray;
  };

  template<>
  inline Vector3DByValarray random<Vector3DByValarray>() {
    fp_t theta = 2.0 * M_PI * randomFloat01();
    fp_t phi = acos(1.0 - 2.0 * randomFloat01());
    fp_t x = sin(phi) * cos(theta);
    fp_t y = sin(phi) * sin(theta);
    fp_t z = cos(phi);
    return {x,y,z};
  }
}