//
// Created by lennard on 7/4/20.
//

#pragma once

#include "geometry/implementations/CoordinatesByValarray.h"

namespace gridbox {

  DEFINE_CONCEPT(Coordinates3DByValarray, CoordinatesByValarray);

  class Coordinates3DByValarray : public CoordinatesByValarray<3> {
  public:
    using Concept = CCoordinates3DByValarray;
    using CoordinatesByValarray<3>::CoordinatesByValarray;

    Coordinates3DByValarray(const fp_t& x, const fp_t& y, const fp_t& z)
    : CoordinatesByValarray({x,y,z})
    {}

    fp_t x() const {
      return _coordinates[0];
    }
    fp_t y() const {
      return _coordinates[1];
    }
    fp_t z() const {
      return _coordinates[2];
    }
  };
}