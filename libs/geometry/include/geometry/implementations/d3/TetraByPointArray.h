//
// Created by Lennard Ockenfels on 7/3/20.
//

#pragma once

#include "TrianglePolyFacet.h"
#include "geometry/functions/d3/Tetra.h"
#include "geometry/implementations/PointSetByArray.h"

namespace gridbox {

  DEFINE_CONCEPT(TetraByPointArray, Tetra, PointSetByArray);

  class TetraByPointArray : public PointSetByArray<Point3DByValarray, 4> {
  public:
    using Concept = CTetraByPointArray;
    using Facet = TrianglePolyFacetByArray;
    using AABB = AABox3DByMinMax;
    using Ray = Ray3DWithPrecomputation;
    using LineSegment = LineSegment3DByArray;

    TetraByPointArray(const Point& p0, const Point& p1, const Point& p2, const Point& p3)
    : PointSetByArray({p0,p1,p2,p3})
    {}
    TetraByPointArray(const std::array<Point,4>& points)
    : PointSetByArray(points)
    {}
  };

}