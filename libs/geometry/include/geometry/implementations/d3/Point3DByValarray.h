//
// Created by Lennard Ockenfels on 5/27/20.
//

#pragma once

#include "Vector3DByValarray.h"
#include "geometry/functions/Point.h"

namespace gridbox {

  DEFINE_CONCEPT(Point3DByValarray, Point, Coordinates3DByValarray);

  class Point3DByValarray : public Coordinates3DByValarray {
  public:
    using Concept = CPoint3DByValarray;
    using Vector = Vector3DByValarray;

    using Coordinates3DByValarray::Coordinates3DByValarray;
  };
}