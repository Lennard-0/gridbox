//
// Created by lennard on 6/29/20.
//

#pragma once

#include "PointSet.h"
#include "geometry/interfaces/LineSegment.h"

namespace gridbox {

  template<LineSegmentC LineSegment>
  inline typename LineSegment::Point pointP(const LineSegment& ls) {
    return points(ls)[0];
  }

  template<LineSegmentC LineSegment>
  inline typename LineSegment::Point pointQ(const LineSegment& ls) {
    return points(ls)[1];
  }

  template<LineSegmentC LineSegment>
  inline bool parallel(const LineSegment& a, const LineSegment& b) {
    return parallel(pointP(a) - pointQ(a), pointP(b) - pointQ(b));
  }

  template<LineSegmentC LineSegment, PointSetC PointSet>
  inline auto intersectionLambdas(const LineSegment& ls,
                                  const PointSet&    pointSet)
                                  -> std::vector<fp_t> {
    auto lambdas = intersectionLambdas(lsRay(ls), pointSet);
    auto it = std::remove_if(lambdas.begin(), lambdas.end(),
                             [](const fp_t& lambda) { return lambda < EPSILON || lambda > 1-EPSILON; });
    lambdas.erase(it, lambdas.end());
    return lambdas;
  }
  template<LineSegmentC LineSegment, PointSetC PointSet>
  inline auto intersectionLambdasInclusive(const LineSegment& ls,
                                           const PointSet&    pointSet)
                                           -> std::vector<fp_t>{
    auto lambdas = intersectionLambdasInclusive(lsRay(ls), pointSet);        // inclusive regarding pointSet
    auto it = std::remove_if(lambdas.begin(), lambdas.end(),                 // todo: differentiate between these two
                             [](const fp_t& lambda) { return lambda > 1; });  // inclusive regarding lineSegment
    lambdas.erase(it, lambdas.end());
    return lambdas;
  }

  template<LineSegmentC LineSegment, PointSetC PointSet>
  inline auto intersectionPoints(const LineSegment& ls,
                                 const PointSet&    pointSet)
                                 -> std::vector<typename PointSet::Point> {
    return rayPoints(lsRay(ls), intersectionLambdas(ls, pointSet));
  }
  template<LineSegmentC LineSegment, PointSetC PointSet>
  inline auto intersectionPointsInclusive(const LineSegment& ls,
                                          const PointSet&    pointSet)
                                          -> std::vector<typename PointSet::Point> {
    return rayPoints(lsRay(ls), intersectionLambdasInclusive(ls, pointSet));
  }

  template<LineSegmentC LineSegment>
  inline typename LineSegment::Ray lsRay(const LineSegment& ls) {
    return {pointP(ls), pointQ(ls) - pointP(ls) };
  }

  template<LineSegmentC LineSegmentA, LineSegmentC LineSegmentB>
  inline bool intersect(const LineSegmentA& a, const LineSegmentB& b) {
    auto lambdaRayLS = intersectionLambdasRayLSInclusive(lsRay(a), b);
    return lambdaRayLS
           && lambdaRayLS->first  > EPSILON && lambdaRayLS->first  < 1 -EPSILON
           && lambdaRayLS->second > EPSILON && lambdaRayLS->second < 1 -EPSILON;
  }

  template<LineSegmentC LineSegment>
  inline std::vector<fp_t> intersectionLambdasInclusive(const typename LineSegment::Ray&  ray,
                                                        const LineSegment&                ls) {
    auto lambdaRayLS = intersectionLambdasRayLSInclusive(ray, ls);
    return lambdaRayLS ? std::vector<fp_t>{ lambdaRayLS->first } : std::vector<fp_t>{};
  }

  template<LineSegmentC LineSegment>
  std::ostream& operator<<(std::ostream& o, const LineSegment& ls) {
    o << "{" << pointP(ls) << "->" << pointQ(ls) << "}";
    return o;
  }
}