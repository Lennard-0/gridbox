//
// Created by lennard on 6/29/20.
//

#pragma once

#include "ConvexPolytope.h"
#include "geometry/interfaces/AABox.h"
#include "geometry/interfaces/Concepts.h"

namespace gridbox {


  template<AABoxC AABox>
  inline bool intersect(const typename AABox::Ray& ray, const AABox& aab) {

    return intersectionLambdaIn(ray, aab).has_value();
  }

  template<AABoxC AABox>
  inline auto intersectionLambdaIn(const typename AABox::Ray&   ray,
                                   const AABox&                 aab) -> std::optional<fp_t> {

    RAY_INTERSECT(AABox);
    using Point = typename AABox::Point;
    using Vector = typename AABox::Vector;
    Point rayOrigin = origin(ray);
    auto rayDirection = direction(ray);

    if (containsInInterior(aab, rayOrigin))
      return 0;

    std::array<fp_t, dimensions(rayOrigin)> t;
    for (int i = 0; i < dimensions(rayOrigin); i++) {
      if (get(rayDirection, i) > 0)
        t[i] = (get(minPoint(aab),i) - get(rayOrigin, i)) * get(mulInvDirection(ray), i);
      else if (get(rayDirection, i) < 0)
        t[i] = (get(maxPoint(aab),i) - get(rayOrigin, i)) * get(mulInvDirection(ray), i);
      else
        t[i] = -INFINITY;
    }
    size_t maxIndex = std::distance(t.begin(), std::max_element(t.begin(), t.end()));
    fp_t maxT = t[maxIndex];
    if (maxT < 0)
      return std::nullopt; //only allow intersection in front of origin count or exactly at origin

    auto intersectionPoint = rayOrigin + maxT * rayDirection;
    for (int i = 1; i < dimensions(rayOrigin); i++) {
      int dimIndex = (maxIndex + i) % dimensions(rayOrigin);
      if (get(intersectionPoint, dimIndex) <  get(minPoint(aab),dimIndex) ||
          get(intersectionPoint, dimIndex) == get(minPoint(aab),dimIndex) && get(rayDirection, dimIndex) < 0 ||
          get(intersectionPoint, dimIndex) >  get(maxPoint(aab),dimIndex) ||
          get(intersectionPoint, dimIndex) == get(maxPoint(aab),dimIndex) && get(rayDirection, dimIndex) > 0)
        return std::nullopt;
    }

    return maxT;
  }

  template<AABoxC AABox>
  inline bool contains(const AABox& aab, const typename AABox::Point& point) {
    auto min = minPoint(aab);
    auto max = maxPoint(aab);
    for(u_short d=0; d<dimensions(point); d++)
      if( get(min, d) > get(point, d)  ||  get(max,d) < get(point,d) )
        return false;

    return true;
  }

  template<AABoxC AABox>
  inline bool containsInInterior(const AABox& aab, const typename AABox::Point& point) {
    auto min = minPoint(aab);
    auto max = maxPoint(aab);
    for(u_short d=0; d<dimensions(point); d++)
      if( get(min, d) >= get(point, d)  ||  get(max,d) <= get(point,d) )
        return false;

    return true;
  }

  template<AABoxC AABox>
  inline typename AABox::Point midPoint(const AABox& aab) {
    return mid(minPoint(aab), maxPoint(aab));
  }

  template<AABoxC AABox>
  std::ostream& operator<<(std::ostream& o, const AABox& aab) {
    o << "{" << minPoint(aab) << "<" << maxPoint(aab) << "}";
    return o;
  }
}