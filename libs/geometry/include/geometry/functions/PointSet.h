//
// Created by lennard on 7/5/20.
//

#pragma once

#include "geometry/interfaces/PointSet.h"
#include "Point.h"

namespace gridbox {

  template<PointSetC PointSet>
  inline bool intersect(const typename PointSet::Ray& ray, const PointSet& pointSet) {
    return intersectionLambdas(ray, pointSet).size();
  }

  template<PointSetC PointSet>
  inline bool intersectInclusive(const typename PointSet::Ray& ray, const PointSet& pointSet) {
    return intersectionLambdasInclusive(ray, pointSet).size();
  }

  template<PointSetC PointSet>
  inline auto intersectionPoints(const typename PointSet::Ray&  ray,
                                 const PointSet&                pointSet)
                                 -> std::vector<typename PointSet::Point> {
    return rayPoints(ray, intersectionLambdas(ray, pointSet));
  }
  template<PointSetC PointSet>
  inline auto intersectionPointsInclusive(const typename PointSet::Ray&  ray,
                                          const PointSet&                pointSet)
                                          -> std::vector<typename PointSet::Point>{
    return rayPoints(ray, intersectionLambdasInclusive(ray, pointSet));
  }
  
  template<PointSetC PointSet>
  inline typename PointSet::AABB aabb(const PointSet& pointSet) {
    return aabb<typename PointSet::AABB>(points(pointSet));
  }
  
  template<PointSetC PointSet>
  inline typename PointSet::AABB aabb(const std::vector<PointSet>& pointSets) {
    std::vector<typename PointSet::Point> allPoints;
    for(const PointSet& pointSet : pointSets)
      for(auto const& point : points(pointSet))
        allPoints.emplace_back(point);
    return typename PointSet::AABB(allPoints);
  }

  template<PointSetC PointSet>
  inline bool operator==(const PointSet& lhs, const PointSet& rhs) {
    auto lhsPoints = points(lhs);
    auto rhsPoints = points(rhs);
    if(lhsPoints.size() != rhsPoints.size())
      return false;
    for(u_short i=0; i<lhsPoints.size(); i++)
      if(lhsPoints[i] != rhsPoints[i])
        return false;
    return true;
  }

  template<PointSetC PointSet>
  inline PointSet fromJson(const json& pointSetJSON) {
    using Point = typename PointSet::Point;
    std::array<Point, PointSet::NumPoints> points;
    auto pointsJSON = pointSetJSON["points"];
    for(u_short i=0; i<PointSet::NumPoints; i++)
      points[i] = fromJson<Point>(pointsJSON[i]);
    return points;
  }
}