//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/functions/AABox.h"
#include "geometry/interfaces/d2/Concepts.h"

namespace gridbox {

  template<AABox2DC AABox>
  inline std::vector<typename AABox::Point> points(const AABox& aab){ //todo: AABox2D::points und AABox3D::points allgemein als AABox::points implementieren
    using Point = typename AABox::Point;
    auto min = minPoint(aab);
    auto max = maxPoint(aab);
    Point sw = min;
    Point se = {max[0],min[1]};
    Point ne = max;
    Point nw = {min[0],max[1]};

    return { sw, se, ne, nw };
  }

  template<AABox2DC AABox>
  inline std::vector<typename AABox::Facet> facets(const AABox& aab){
    auto pts = points(aab);
    return {{
              {pts[0], pts[1]},
              {pts[1], pts[2]},
              {pts[2], pts[3]},
              {pts[3], pts[0]}
            }};
  }

  template<AABox2DC AABox>
  inline std::array<AABox, AABox::num_orthants> orthants(const AABox& aab) {
    using Point = typename AABox::Point;
    auto min = minPoint(aab);
    auto max = maxPoint(aab);
    auto mid = midPoint(aab);
    Point wm = {min[0],mid[1]};
    Point nm = {mid[0],max[1]};
    Point em = {max[0],mid[1]};
    Point sm = {mid[0],min[1]};

    return {{
      {min, mid},
      {wm,  nm},
      {sm,  em},
      {mid, max}
    }};
  }
}