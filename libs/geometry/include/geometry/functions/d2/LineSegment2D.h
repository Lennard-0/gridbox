//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/functions/LineSegment.h"
#include "geometry/interfaces/d2/Concepts.h"

namespace gridbox {

  template<LineSegment2DC LineSegment>
  inline auto intersectionLambdasRayLSInclusive(const typename LineSegment::Ray&  ray,
                                                const LineSegment&                ls)
                                                -> std::optional<std::pair<fp_t,fp_t>> {
    using Point = typename LineSegment::Point;
    using Vector = typename LineSegment::Vector;
    Point ls0 = pointP(ls);
    Vector lsD = pointQ(ls) - pointP(ls);
    Point ray0 = ray.origin();
    Vector rayD = ray.direction();
    // solving ls0 + lT * lsD = ray0 + rT * rayD

    if(parallel(lsD,rayD))
      return std::nullopt;

    fp_t rT = (ray0.x() * lsD.y() - ls0.x() * lsD.y() + ls0.y() * lsD.x() - ray0.y() * lsD.x()) / (rayD.y() * lsD.x() - rayD.x() * lsD.y() );
    if(rT < -EPSILON)
      return std::nullopt;

    fp_t lT;
    if(std::abs(lsD.x()) > EPSILON)
      lT = (ray0.x() + rT * rayD.x() - ls0.x() ) / lsD.x();
    else
      lT = (ray0.y() + rT * rayD.y() - ls0.y() ) / lsD.y();
    if(lT < -EPSILON || lT > 1 + EPSILON)
      return std::nullopt;

    return std::make_pair(rT, lT);
  }
}