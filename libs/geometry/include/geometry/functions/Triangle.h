//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/interfaces/Triangle.h"

namespace gridbox {

  template<TriangleC Triangle>
  inline typename Triangle::Point pointA(const Triangle& triangle) {
    return points(triangle)[0];
  }
  template<TriangleC Triangle>
  inline typename Triangle::Point pointB(const Triangle& triangle) {
    return points(triangle)[1];
  }
  template<TriangleC Triangle>
  inline typename Triangle::Point pointC(const Triangle& triangle) {
    return points(triangle)[2];
  }

  template<TriangleC Triangle>
  inline std::vector<typename Triangle::LineSegment> lineSegments(const Triangle& triangle) {
    auto trianglePoints = points(triangle);
    return {
      { trianglePoints[0], trianglePoints[1] },
      { trianglePoints[1], trianglePoints[2] },
      { trianglePoints[2], trianglePoints[0] }
    };
  }

  template<TriangleC Triangle>
  std::ostream& operator<<(std::ostream& o, const Triangle& triangle) {
    o << "Triangle{" << pointA(triangle) << ", " << pointB(triangle) << ", " << pointC(triangle) << "}";
    return o;
  }
}