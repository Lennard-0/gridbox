//
// Created by lennard on 6/29/20.
//

#pragma once

#include "geometry/interfaces/Ray.h"

namespace gridbox {

  template<RayC Ray>
  inline typename Ray::Point rayPoint(const Ray& ray, const fp_t& lambda) {
    return origin(ray) + lambda * direction(ray);
  }

  template<RayC Ray>
  inline std::vector<typename Ray::Point> rayPoints(const Ray& ray, const std::vector<fp_t>& lambdas) {
    std::vector<typename Ray::Point> result;
    result.reserve(lambdas.size());

    std::generate_n(std::back_inserter(result), lambdas.size(),
                    [i=0,&ray,&lambdas]() mutable { return rayPoint(ray, lambdas[i++]); });
    return result;
  }

  template<RayC Ray>
  std::ostream& operator<<(std::ostream& o, const Ray& ray) {
    o << "{" << origin(ray) << " -> " << direction(ray) << "}";
    return o;
  }

  template<RayC Ray>
  inline json toJson(const Ray& ray) {
    json result = {};
    result["origin"] = toJson(origin(ray));
    result["direction"] = toJson(direction(ray));
    return result;
  }
}