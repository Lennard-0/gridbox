//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/functions/AABox.h"
#include "geometry/interfaces/d3/Concepts.h"

#include <numbers>
double cot(const double& angle) {
  double x = angle * std::numbers::pi / 180.0; //convert to radian
  return cos(x) / sin(x);
}

namespace gridbox {

  template<AABox3DC AABox>
  inline std::vector<typename AABox::Point> points(const AABox& aab) {
    auto min = minPoint(aab);
    auto max = maxPoint(aab);
    return {
      min, {max[0],min[1],min[2]}, {max[0],max[1],min[2]}, {min[0],max[1],min[2]},
      {min[0],min[1],max[2]}, {max[0],min[1],max[2]}, max, {min[0],max[1],max[2]}
    };
  }

  template<AABox3DC AABox>
  inline std::vector<typename AABox::Facet> facets(const AABox& aab) {
    using Point = typename AABox::Point;
    auto min = minPoint(aab);
    auto max = maxPoint(aab);
    Point swBack  = min;
    Point seBack  = {max[0],min[1],min[2]};
    Point neBack  = {max[0],max[1],min[2]};
    Point nwBack  = {min[0],max[1],min[2]};
    Point swFront = {min[0],min[1],max[2]};
    Point seFront = {max[0],min[1],max[2]};
    Point neFront = max;
    Point nwFront = {min[0],max[1],max[2]};
    return {{
              {0,true, swBack, nwFront}, //west
              {0,false,seBack, neFront}, //east
              {1,true, swBack, seFront}, //south
              {1,false,nwBack, neFront}, //north
              {2,true, swBack, neBack},  //back
              {2,false,swFront,neFront}, //front
            }};
  }

  template<AABox3DC AABox>
  inline std::array<AABox, AABox::num_orthants> orthants(const AABox& aab) {
    using Point = typename AABox::Point;
    auto min = minPoint(aab);
    auto max = maxPoint(aab);
    auto mid = midPoint(aab);

    Point wmBack  = {min[0],mid[1],min[2]};
    Point mnMid   = {mid[0],max[1],mid[2]};
    Point wsMid   = {min[0],min[1],mid[2]};
    Point mmFront = {mid[0],mid[1],max[2]};
    Point wmMid   = {min[0],mid[1],mid[2]};
    Point mnFront = {mid[0],max[1],max[2]};
    Point msBack  = {mid[0],min[1],min[2]};
    Point emMid   = {max[0],mid[1],mid[2]};
    Point msMid   = {mid[0],min[1],mid[2]};
    Point emFront = {max[0],mid[1],max[2]};
    Point mmBack  = {mid[0],mid[1],min[2]};
    Point enMid   = {max[0],max[1],mid[2]};

    return {{
      {min,    mid},
      {wsMid,  mmFront},
      {wmBack, mnMid},
      {wmMid,  mnFront},
      {msBack, emMid},
      {msMid,  emFront},
      {mmBack, enMid},
      {mid,    max}
    }};
  }

  template<AABox3DC AABox>
  inline std::vector<typename AABox::Ray> createRays(const AABox& aaBox, const uint& width, const uint& height) {
    double fov = 70;
    double screenDistance = 10;
    double screenWidth = tan((fov/2) * std::numbers::pi / 180.0) * screenDistance * 2;
    double screenHeight = screenWidth * height/width;
    double verticalFOV = ( atan( (screenHeight/2.0) / screenDistance ) * 180.0 / std::numbers::pi ) * 2;
    LOG(INFO) << "resolution: " << width << "x" << height
              << " virtual screen: " << screenWidth << "x" << screenHeight
              << " FOV: " << fov << "x" << verticalFOV
              << " (WIDTHxHEIGHT)";

    auto boxMid = midPoint(aaBox);
    auto sphereR = norm2(maxPoint(aaBox) - midPoint(aaBox));
    auto distanceToMid = cot(fov/2) * sphereR;

    auto cameraPosition = boxMid + typename AABox::Vector(0,0,distanceToMid);
    typename AABox::Vector cameraDirection = {0, 0, -1};

    double xAngleOffset = fov / (width-1);
    double yAngleOffset = verticalFOV / (height-1);


    double xAngle = - fov/2;
    std::vector<fp_t> xValues(width);
    for(auto& x : xValues) {
      if(xAngle == 0)
        x = 0;
      else
        x = tan(xAngle * std::numbers::pi / 180.0);
      xAngle += xAngleOffset;
    }
    double yAngle = - verticalFOV/2;
    std::vector<fp_t> yValues(height);
    for(auto& y : yValues) {
      if(yAngle == 0)
        y = 0;
      else
        y = tan(yAngle * std::numbers::pi / 180.0);
      yAngle += yAngleOffset;
    }

    std::vector<typename AABox::Ray> result;
    result.reserve(width*height);
    for(uint y=0; y<height; y++)
      for(uint x=0; x<width; x++) {
        result.emplace_back(typename AABox::Ray{cameraPosition, {xValues[x], yValues[y], -1}});
      }
    return result;
  }
}