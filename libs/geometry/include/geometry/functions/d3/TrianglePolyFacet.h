//
// Created by lennard on 7/13/20.
//

#pragma once

#include "geometry/functions/Triangle.h"
#include "geometry/interfaces/d3/Concepts.h"

namespace gridbox {

  // based on the Möller–Trumbore intersection algorithm
  template<TrianglePolyFacetC TrianglePolyFacet>
  inline std::vector<fp_t> intersectionLambdasInclusive(const typename TrianglePolyFacet::Ray& ray,
                                                        const TrianglePolyFacet& triangle) {
    using Vector = typename TrianglePolyFacet::Vector;
    auto pts = points(triangle);

    Vector edge1, edge2, h, s, q;
    fp_t a,f,u,v;
    edge1 = pts[1] - pts[0];
    edge2 = pts[2] - pts[0];
    h = crossProduct(ray.direction(), edge2);
    a = dotProduct(edge1, h);
    if (a > -EPSILON && a < EPSILON)
      return {};   //ray is parallel to the triangle
    f = 1.0/a;
    s = ray.origin() - pts[0];
    u = f * dotProduct(s, h);
    if (u < 0.0 || u > 1.0)
      return {};
    q = crossProduct(s, edge1);
    v = f * dotProduct(ray.direction(), q);
    if (v < 0.0 || u + v > 1.0)
      return {};
    // At this stage we can compute lambda to find out where the intersection point is on the line.
    float lambda = f * dotProduct(edge2,q);

    if (lambda < -EPSILON)
      return {}; // intersection behind origin
    return {lambda};
  }

  template<TrianglePolyFacetC TrianglePolyFacet, AARectangleC AARectangle>
  inline bool intersect(const TrianglePolyFacet& triangle, const AARectangle& aaRec) {
    for(auto const& ls : lineSegments(triangle))
      if(intersect(ls,aaRec))
        return true;

    for(auto const& ls : lineSegments(aaRec)) {
      auto lsT = intersectionLambdasInclusive(lsRay(ls), triangle);
      if(lsT.size() && lsT[0] > EPSILON && lsT[0] < 1 -EPSILON)
        return true;
    }

    return false;
  }
}