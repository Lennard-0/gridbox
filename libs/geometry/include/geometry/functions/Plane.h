//
// Created by lennard on 7/4/20.
//

#pragma once

#include "geometry/interfaces/Plane.h"

namespace gridbox {

  template<PlaneC Plane>
  inline bool isLeft(const typename Plane::Point& point, const Plane& plane) {
    auto supportPointVec = point - support(plane);
    return dotProduct(supportPointVec, normal(plane)) < -EPSILON; //true -> angle between normal and dir > 90
  }
  template<PlaneC Plane>
  inline bool isLeftOrOn(const typename Plane::Point& point, const Plane& plane) {
    auto supportPointVec = point - support(plane);
    return dotProduct(supportPointVec, normal(plane)) < EPSILON; //true -> angle between normal and dir >= 90
  }

  template<PlaneC Plane>
  std::ostream& operator<<(std::ostream& o, const Plane& plane) {
    o << "{s" << support(plane) << ", n" << normal(plane) << "}";
    return o;
  }
}