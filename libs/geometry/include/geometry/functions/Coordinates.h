//
// Created by lennard on 6/28/20.
//

#pragma once

#include "geometry/interfaces/Coordinates.h"

namespace gridbox {

  template<CoordinatesC Coordinates>
  constexpr u_short dimensions(const Coordinates& coords) {
    return Coordinates::DIMENSIONS;
  }

  template<CoordinatesC Coordinates>
  inline fp_t get(const Coordinates& coords, u_short i){
    return coordinates(coords)[i];
  }

  template<CoordinatesC Coordinates>
  bool operator==(const Coordinates& lhs, const Coordinates& rhs) {
    return (coordinates(lhs) == coordinates(rhs)).min();
  }

  template<CoordinatesC Coordinates>
  std::ostream& operator<<(std::ostream& o, const Coordinates& coords) {
    o << "(" << get(coords, 0);
    for (u_short i = 1; i < dimensions(coords); i++)
      o << ", " << get(coords, i);
    o << ")";
    return o;
  }

  template<CoordinatesC Coordinates>
  struct lex_compare {
    bool operator() (const Coordinates& lhs, const Coordinates& rhs) const {
      std::stringstream s1, s2;
      s1 << lhs;
      s2 << rhs;
      return s1.str() < s2.str();
      //todo: use std::lexicographical_compare
    }
  };

  inline json toJson(const std::valarray<fp_t>& coords) {
    json result = json::array();
    for(auto const& el : coords)
      result += el;
    return result;
  }

  inline std::valarray<fp_t> vaFromJson(const json& vaJSON) {
    std::valarray<fp_t> result(vaJSON.size());
    for(u_int i=0; i<vaJSON.size(); i++)
      result[i] = vaJSON.at(i).get<fp_t>();
    return result;
  }

  template<CoordinatesC Coordinates>
  inline json toJson(const Coordinates& coords) {
    json result = {};
    result["coordinates"] = toJson(coordinates(coords));
    return result;
  }

  template<CoordinatesC Coordinates>
  inline Coordinates fromJson(const json& coordsJSON) {
    return vaFromJson(coordsJSON["coordinates"]);
  }
}
