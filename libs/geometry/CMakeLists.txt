
add_library(geometry
        include/geometry/interfaces/Vector.h
        include/geometry/interfaces/Vector.h
        include/geometry/interfaces/Coordinates.h
        include/geometry/interfaces/LineSegment.h
        include/geometry/interfaces/Ray.h
        include/geometry/interfaces/AABox.h
        include/geometry/interfaces/ConvexPolytope.h
        include/geometry/interfaces/PolyFacet.h
        include/geometry/interfaces/Plane.h
        include/geometry/interfaces/Concepts.h
        include/geometry/interfaces/Sphere.h
        include/geometry/interfaces/d2/Concepts.h
        include/geometry/interfaces/d3/Vector3D.h
        include/geometry/interfaces/d3/Concepts.h
        include/geometry/interfaces/d3/AARectangle.h

        include/geometry/functions/Point.h
        include/geometry/functions/Vector.h
        include/geometry/functions/Coordinates.h
        include/geometry/functions/LineSegment.h
        include/geometry/functions/AABox.h
        include/geometry/functions/Ray.h
        include/geometry/functions/ConvexPolytope.h
        include/geometry/functions/Plane.h
        include/geometry/functions/PointSet.h

        include/geometry/functions/d2/LineSegment2D.h
        include/geometry/functions/d2/AABox2D.h

        include/geometry/functions/d3/Vector3D.h
        include/geometry/functions/d3/AABox3D.h
        include/geometry/functions/d3/AARectangle.h
        include/geometry/functions/d3/Tetra.h
        include/geometry/functions/d3/TrianglePolyFacet.h

        include/geometry/implementations/CoordinatesByValarray.h
        include/geometry/implementations/PointSetByArray.h
        include/geometry/implementations/PlaneByPointAndVector.h
        include/geometry/implementations/AABoxByMinMax.h
        include/geometry/implementations/RayWithPrecomputation.h
        include/geometry/implementations/d2/Triangle2DByArray.h
        include/geometry/implementations/d2/LineSegment2DByArray.h
        include/geometry/implementations/d2/Point2DByValarray.h
        include/geometry/implementations/d2/LineSegmentPolyFacet.h
        include/geometry/implementations/d2/Plane2D.h
        include/geometry/implementations/d2/Ray2DWithPrecomputation.h
        include/geometry/implementations/d2/Coordinates2DByValarray.h
        include/geometry/implementations/d3/AARectanglePolyFacet.h
        include/geometry/implementations/d3/TetraByPointArray.h
        include/geometry/implementations/SphereByPoint.h
        include/geometry/implementations/d2/CirclePyPoint.h
        include/geometry/implementations/d3/Sphere3D.h)

target_compile_options(geometry PRIVATE -Werror=return-type)
target_include_directories(geometry PUBLIC include)
target_compile_features(geometry PUBLIC cxx_std_20)
target_link_libraries(geometry PUBLIC Half core)