//
// Created by lennard on 6/30/20.
//

#pragma once

#include "Primitive.h"

namespace gridbox {

  class CGrid {};
  template<typename T>
  concept GridC = std::is_base_of_v<CGrid, typename T::Concept>;

  template<GridC Grid>
  inline std::vector<typename Grid::Primitive> primitives(const Grid& grid);

  template<GridC Grid>
  inline void loadTestData(Grid& grid);

  template<GridC Grid>
  inline void loadFromFile(Grid& grid, const std::string& filename);

  template<typename T>
  std::valarray<u_short> regionSizes(const std::vector<T>& regions);
}