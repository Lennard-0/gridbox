//
// Created by lennard on 7/10/20.
//

#pragma once

#include <valarray>

namespace gridbox {

  DEFINE_CONCEPT(Region);

  template<RegionC Region>
  typename Region::size_type size(const Region& region);

  //todo: require Region::size_type
  //todo: require region.size()
  //todo: require region.emplace_back(T)
  //todo: require Region to be iterable or to be a Container?

  template<RegionC Region>
  std::valarray<typename Region::size_type> regionSizes(const std::vector<Region>& regions);
}