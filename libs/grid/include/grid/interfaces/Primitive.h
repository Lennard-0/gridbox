//
// Created by lennard on 6/30/20.
//

#pragma once

#include <geometry/interfaces/ConvexPolytope.h>

namespace gridbox {

  DEFINE_CONCEPT(Primitive, ConvexPolytope);

  template<PrimitiveC Primitive>
  inline std::vector<typename Primitive::Neighbor> neighbors(const Primitive& primitive);
  template<PrimitiveC Primitive>
  inline typename Primitive::Neighbor neighbor(const Primitive& primitive, const FacetID& facetID);
  template<PrimitiveC Primitive>
  inline typename Primitive::Data data(const Primitive& primitive);
}