//
// Created by lennard on 6/30/20.
//

#pragma once

#include "geometry/implementations/d2/Triangle2DByArray.h"
#include "grid/implementations/PrimitiveWithFloat.h"

namespace gridbox {

  DEFINE_CONCEPT(TrianglePrimitiveWithFloat, Triangle2DByArray, PrimitiveWithFloat);

  class TrianglePrimitive : public PrimitiveWithFloat<Triangle2DByArray> {
  public:
    using Concept = CTrianglePrimitiveWithFloat;
    using ConvexPolytope = Triangle2DByArray;
  };
}