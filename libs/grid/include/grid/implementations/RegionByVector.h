//
// Created by lennard on 7/23/20.
//

#pragma once

#include "grid/functions/Region.h"

namespace gridbox {

  DEFINE_CONCEPT(RegionByVector, Region);

  template<PrimitiveC PrimitiveT>
  class RegionByVector {
  public:
    using Concept = CRegionByVector;
    using size_type = u_short;
    using Primitive = PrimitiveT;
    using Ray = typename Primitive::Ray;
    using iterator=typename std::vector<Primitive>::iterator;
    using const_iterator=typename std::vector<Primitive>::const_iterator;

    using reference=typename std::iterator_traits<typename std::vector<Primitive>::iterator>::reference;
    using const_reference=typename std::iterator_traits<typename std::vector<Primitive>::const_iterator>::reference;

    RegionByVector() = default;
    RegionByVector(const std::vector<Primitive>& primitives)
    : _primitives(primitives)
    {}

    iterator begin() { return _primitives.begin(); }
    iterator end() { return _primitives.end(); }
    const_iterator begin() const { return _primitives.begin(); }
    const_iterator end() const { return _primitives.end(); }

    reference emplace_back(const_reference primitive) {
      return _primitives.emplace_back(primitive);
    }

    u_short size() const {
      return _primitives.size();
    }

    constexpr reference operator[]( size_type pos ) {
      return _primitives[pos];
    }

    constexpr const_reference operator[]( size_type pos ) const {
      return _primitives[pos];
    }

  private:
    std::vector<Primitive> _primitives;
  };

  template<RegionByVectorC Region>
  inline json toJson(const Region& region) {
    json result = {};
    json primitivesJSON = json::array();
    for(auto const& pr : region)
      primitivesJSON += toJson(pr);
    result["primitives"] = primitivesJSON;
    return result;
  }
}