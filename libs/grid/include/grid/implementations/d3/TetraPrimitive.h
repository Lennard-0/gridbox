//
// Created by lennard on 6/30/20.
//

#pragma once

#include "geometry/implementations/d3/TetraByPointArray.h"
#include "grid/implementations/PrimitiveWithFloat.h"

namespace gridbox {

  DEFINE_CONCEPT(TetraPrimitive, TetraByPointArray, PrimitiveWithFloat);

  class TetraPrimitive : public PrimitiveWithFloat<TetraByPointArray> {
  public:
    using Concept = CTetraPrimitive;
    using ConvexPolytope = TetraByPointArray;

    using PrimitiveWithFloat<TetraByPointArray>::PrimitiveWithFloat;
  };
}