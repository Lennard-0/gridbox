//
// Created by lennard on 7/10/20.
//

#pragma once

#include "grid/interfaces/Region.h"
#include "Primitive.h"
#include <valarray>

namespace gridbox {

  template<RegionC Region>
  typename Region::size_type size(const Region& region) {
    return region.size();
  }

  template<RegionC Region>
  std::valarray<typename Region::size_type> regionSizes(const std::vector<Region>& regions) {
    std::valarray<typename Region::size_type> result(regions.size());
    for(typename Region::size_type i=0; i<regions.size(); i++)
      result[i] = size(regions[i]);
    return result;
  }

  template<RegionC Region>
  bool isOrderedSubset(const std::vector<Region>& regionsA, const std::vector<Region>& regionsB) {
    auto rgsBIt = regionsB.begin();
    for(const Region& regionA : regionsA)
      for(auto const& primitiveA : regionA) {
        bool found = false;
        while(rgsBIt != regionsB.end()) {
          for(auto const& primitiveB : *rgsBIt)
            if(primitiveA == primitiveB) {
              found = true;
              break;
            }
          if(found)
            break;
          rgsBIt++;
        }
        if(!found)
          return false;
      }
    return true;
  }

  template<RegionC Region>
  inline Region fromJson(const json& regionJSON) {
    Region result;
    for(auto const& prt : regionJSON["primitives"])
      result.emplace_back(fromJson<typename Region::Primitive>(prt));
    return result;
  }
}