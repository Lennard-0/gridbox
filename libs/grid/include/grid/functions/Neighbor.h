//
// Created by lennard on 6/30/20.
//

#pragma once

#include "grid/interfaces/Neighbor.h"

namespace gridbox {

  template<NeighborC Neighbor>
  inline bool isValid(const Neighbor& neighbor) {
    return facetID(neighbor) != INVALID_FACET_ID;
  }

  template<NeighborC Neighbor>
  std::ostream& operator<<(std::ostream& o, const Neighbor& neighbor) {
    o << "(";
    if(primitiveID(neighbor) == INVALID_PRIMITIVE_ID)
      o << "edge";
    else
      o << primitiveID(neighbor) << "->" << facetID(neighbor);
    o << ")";
    return o;
  }

  template<NeighborC Neighbor>
  inline Neighbor fromJson(const json& nbJSON) {
    return Neighbor(nbJSON["primitiveID"].get<PrimitiveID>(), nbJSON["facetID"].get<FacetID>());
  }
}