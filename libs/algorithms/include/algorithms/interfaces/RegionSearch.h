//
// Created by lennard on 7/27/20.
//

#pragma once

#include <grid/interfaces/Region.h>

namespace gridbox {

  template<RegionC Region>
  inline std::vector<typename Region::Primitive> intersectedPrimitives(const std::vector<Region>&   regions,
                                                                       const typename Region::Ray&  ray);
}