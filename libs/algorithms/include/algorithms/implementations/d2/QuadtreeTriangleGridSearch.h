//
// Created by lennard on 7/10/20.
//

#pragma once

#include "algorithms/implementations/FullMAryTreeGridSearch.h"
#include "grid/implementations/d2/TriangleGrid.h"

namespace gridbox {

  using QuadtreeTriangleGridSearch = FullMAryTreeGridSearch<TriangleGrid>;
}