//
// Created by lennard on 7/10/20.
//

#pragma once

#include "algorithms/implementations/FullMAryTreeGridSearch.h"
#include "grid/implementations/d3/TetraGrid.h"

namespace gridbox {

  using OctreeTetraGridSearch = FullMAryTreeGridSearch<TetraGrid>;
}