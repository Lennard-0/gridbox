//
// Created by lennard on 7/10/20.
//

#pragma once

#include "algorithms/implementations/LinearGridSearch.h"
#include "grid/implementations/d3/TetraGrid.h"

namespace gridbox {

  using LinearTetraGridSearch = LinearGridSearch<TetraGrid>;
}