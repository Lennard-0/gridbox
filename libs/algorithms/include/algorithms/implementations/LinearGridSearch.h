//
// Created by lennard on 6/30/20.
//

#pragma once

#include "algorithms/interfaces/GridSearch.h"
#include <set>

namespace gridbox {

  DEFINE_CONCEPT(LinearGridSearch, GridSearch);

  template<GridC GridT>
  class LinearGridSearch {
  public:
    using Concept = CLinearGridSearch;
    using Grid = GridT;
    using Primitive = typename Grid::Primitive;
    using Region = typename Grid::Region;
    using Regions = std::vector<Region>;
    using AABox = typename Grid::AABox;
    using Ray = typename Primitive::Ray;
    using Point = typename Ray::Point;
    using Vector = typename Point::Vector;

    void construct(const Grid& grid, u_short maxRegionSize) {
      _grid = std::move(grid);
      _maxRegionSize = maxRegionSize;
    }
    std::vector<Region> getIntersectedBy(const Ray& ray) const {
      struct Intersection {
        Primitive primitive;
        fp_t rayLambda;
      };
      struct intersection_compare {
        bool operator() (const Intersection& lhs, const Intersection& rhs) const {
          return lhs.rayLambda < rhs.rayLambda;
        }
      };
      std::set<Intersection, intersection_compare> intersections;

      for(const Primitive& primitive : primitives(_grid))
        if(auto inOut = intersectionLambdasInOut(ray, primitive))
          intersections.emplace(primitive, inOut->first);

      std::vector<Region> regions = {{}};
      for(const Intersection& intersection : intersections) {
        if( regions.back().size() == _maxRegionSize )
          regions.emplace_back(Region{});
        regions.back().emplace_back(intersection.primitive);
      }
      return regions;
    }

  private:
    Grid _grid;
    u_short _maxRegionSize;
  };

  template<LinearGridSearchC LinearGS>
  inline void construct(LinearGS& gridSearch, const typename LinearGS::Grid& grid, u_short maxRegionSize) {
    gridSearch.construct(grid, maxRegionSize);
  }

  template<LinearGridSearchC LinearGS>
  inline std::vector<typename LinearGS::Region> getIntersectedBy(const LinearGS& gridSearch, const typename LinearGS::Ray& ray) {
    return gridSearch.getIntersectedBy(ray);
  }
}