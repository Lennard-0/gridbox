//
// Created by lennard on 7/27/20.
//

#pragma once

#include "algorithms/interfaces/RegionSearch.h"

namespace gridbox {

  template<RegionC Region>
  static fp_t addIntersectedOutLambda(const Region& region, const typename Region::Ray& ray, std::vector<typename Region::Primitive>& result) {
    using SizeType = typename Region::size_type;
    std::vector<std::pair<fp_t, SizeType>> lambdaIDs;
    for(SizeType i=0; i<region.size(); i++) {
      auto lambdaInOut = intersectionLambdasInOut(ray, region[i]);
      if(lambdaInOut)
        lambdaIDs.emplace_back(std::make_pair(lambdaInOut.value().second, i));
    }
    std::sort(lambdaIDs.begin(), lambdaIDs.end());
    for(auto const& lambdaID : lambdaIDs)
      result.emplace_back(region[lambdaID.second]);
    if(lambdaIDs.empty())
      return 0;
    return lambdaIDs.rbegin()->first;
  }

  template<RegionC Region>
  inline std::vector<typename Region::Primitive> intersectedPrimitives(const std::vector<Region>&   regions,
                                                                       const typename Region::Ray&  ray) {
    std::vector<typename Region::Primitive> result;
    typename Region::Ray currRay = ray;
    for(auto const& region : regions) {
      fp_t outLambda = addIntersectedOutLambda(region, currRay, result);
      currRay = { rayPoint(currRay, outLambda), direction(ray) };
    }
    return result;
  }
}