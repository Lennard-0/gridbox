//
// Created by lennard on 7/6/20.
//

#include <gtest/gtest.h>

#include <easylogging++.h>
INITIALIZE_EASYLOGGINGPP

int main(int argc, char **argv) {

  el::Configurations defaultConf;
  defaultConf.setGlobally(el::ConfigurationType::Format, "%level: %msg");
  defaultConf.set(el::Level::Trace, el::ConfigurationType::Enabled, "false");
  el::Loggers::reconfigureLogger("default", defaultConf);

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
