//
// Created by lennard on 7/6/20.
//

#include "TestFigure2D.h"

class LineSegmentPolyFacetTest : public TestFigure2D {
protected:

  Point p = {2,1};
  Point q = {1,2};
  LineSegmentPolyFacet lsA = {p,q};
  Point l = {4,2};
  Point m = {6,0};
  LineSegmentPolyFacet parallelToA = {l,m};
  LineSegmentPolyFacet notParallelToA = {p,l};
  Point k = {1,1};
  LineSegmentPolyFacet intersectsA = {k,l};
  LineSegmentPolyFacet touchesA = {k,mid(p,q)};
  LineSegmentPolyFacet verticalNotIntersecting = {{0,1},{0,0}};

  Ray rayIntersectsA = { k, l-k };
  Ray startsOnA = { mid(p,q), {1,1} };
  Ray rayTouchesA = {0, {4,2}};
  Ray rayMissingA = {0, {4,1}};
  Ray rayBehindA = {2, {1,1}};

  Vector perpendicularToA = {2,2};
  Point left = {1,1};
  Point right = {4,2};
  Point on = {3,0};

  AABox containsA = {{0,0},{3,3}};
  AABox containsOnePoint = {{0,0},{3,1.5}};
  AABox touchesLowerPoint = {{0,0},{3,1}};
};

TEST_F(LineSegmentPolyFacetTest, parallel) {
  ASSERT_TRUE(parallel(lsA, parallelToA));
  ASSERT_FALSE(parallel(lsA, notParallelToA));
}

TEST_F(LineSegmentPolyFacetTest, intersect) {
  ASSERT_TRUE(intersect(lsA, intersectsA));
  ASSERT_FALSE(intersect(lsA, parallelToA));
  ASSERT_FALSE(intersect(lsA, notParallelToA));
  ASSERT_FALSE(intersect(lsA, touchesA));
  ASSERT_FALSE(intersect(touchesA, lsA));
  ASSERT_FALSE(intersect(lsA, verticalNotIntersecting));
}

TEST_F(LineSegmentPolyFacetTest, intersectionPointsInclusive) {
  ASSERT_EQ(intersectionPointsInclusive(rayIntersectsA, lsA).size(), 1);
  ASSERT_EQ(intersectionPointsInclusive(startsOnA, lsA).size(), 1);
  ASSERT_EQ(intersectionPointsInclusive(rayTouchesA, lsA).size(), 1);
  ASSERT_EQ(intersectionPointsInclusive(rayMissingA, lsA).size(), 0);
  ASSERT_EQ(intersectionPointsInclusive(rayBehindA, lsA).size(), 0);
}

TEST_F(LineSegmentPolyFacetTest, plane) {
  Plane result = plane(lsA);

  ASSERT_TRUE(parallel(normal(result), perpendicularToA));

  ASSERT_TRUE(isLeft(left, result));
  ASSERT_TRUE(isLeftOrOn(on, result));
  ASSERT_FALSE(isLeft(on, result));
  ASSERT_FALSE(isLeftOrOn(right, result));
}

TEST_F(LineSegmentPolyFacetTest, intersectAABox) {
  ASSERT_TRUE(intersect(lsA, containsA));
  ASSERT_TRUE(intersect(lsA, containsOnePoint));
  ASSERT_FALSE(intersect(lsA, touchesLowerPoint));
}