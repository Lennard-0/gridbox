//
// Created by lennard on 7/6/20.
//

#pragma once

#include "gtest/gtest.h"
#include <geometry/implementations/d2/Ray2DWithPrecomputation.h>
#include <geometry/implementations/d2/Plane2D.h>
#include <geometry/implementations/d2/AABox2DByMinMax.h>
#include <grid/implementations/d2/TriangleGrid.h>
#include "geometry/implementations/d2/Point2DByValarray.h"
#include "geometry/implementations/d2/LineSegmentPolyFacet.h"

using namespace gridbox;

class TestFigure2D : public ::testing::Test {
protected:
  using Point = Point2DByValarray;
  using Vector = Vector2DByValarray;
  using Ray = Ray2DWithPrecomputation;
  using Plane = Plane2D;
  using AABox = AABox2DByMinMax;
  using Region = typename TriangleGrid::Region;

  void SetUp() override {
    resetCounters();
  }
};