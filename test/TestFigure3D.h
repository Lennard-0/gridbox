//
// Created by lennard on 7/6/20.
//

#pragma once

#include "gtest/gtest.h"
#include <geometry/implementations/d3/Ray3DWithPrecomputation.h>
#include <geometry/implementations/d3/Plane3D.h>
#include <geometry/implementations/d3/AABox3DByMinMax.h>
#include <geometry/implementations/d3/TetraByPointArray.h>
#include "geometry/implementations/d3/Point3DByValarray.h"
#include "grid/implementations/d3/TetraGrid.h"

using namespace gridbox;

class TestFigure3D : public ::testing::Test {
protected:
  using Point = Point3DByValarray;
  using Vector = Vector3DByValarray;
  using Ray = Ray3DWithPrecomputation;
  using Plane = Plane3D;
  using AABox = AABox3DByMinMax;
  using Tetra = TetraByPointArray;
  using Triangle = TrianglePolyFacetByArray;
  using Region = typename TetraGrid::Region;

  void SetUp() override {
    resetCounters();
  }

};