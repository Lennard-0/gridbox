//
// Created by lennard on 7/6/20.
//

#include "TestFigure3D.h"


class Vector3DTest : public TestFigure3D {
protected:

  Vector a = {0,-1,2};
  Vector aa = {0,-1,2};
  Vector b = {2,3,4};
  Vector aPlusB = {2,2,6};
  Vector aHalf = {0,-0.5,1};
  Vector parallelToA = {0,-3,6};
  fp_t dotProductAB = 5;
  Vector crossProductAB = {-10,4,2};
};


TEST_F(Vector3DTest, dimensions) {
  auto expected = Vector::DIMENSIONS;
  ASSERT_EQ(dimensions(a), expected);
}

TEST_F(Vector3DTest, get) {
  ASSERT_EQ(get(a, 0), 0);
  ASSERT_EQ(get(a, 1),-1);
  ASSERT_EQ(get(a, 2), 2);
}

TEST_F(Vector3DTest, coordinates) {
  std::valarray<fp_t> result = coordinates(a);
  for(u_short i=0; i<dimensions(a); i++)
    ASSERT_EQ(get(a, i), result[i]);
}

TEST_F(Vector3DTest, equality) {
  ASSERT_EQ(a, aa);
  ASSERT_NE(a, b);
}

TEST_F(Vector3DTest, scalarProduct) {
  ASSERT_EQ(0.5 * a, aHalf);
}

TEST_F(Vector3DTest, division) {
  ASSERT_EQ(a / 2, aHalf);
}

TEST_F(Vector3DTest, plus) {
  ASSERT_EQ(a + b, aPlusB);
}

TEST_F(Vector3DTest, minus) {
  ASSERT_EQ(a - b, a + -1 * b);
}

TEST_F(Vector3DTest, less) {
  ASSERT_TRUE(a < b);
  ASSERT_FALSE(b < a);
  ASSERT_FALSE(a < a);
}

TEST_F(Vector3DTest, greater) {
  ASSERT_TRUE(b > a);
  ASSERT_FALSE(a > b);
  ASSERT_FALSE(a > a);
}

TEST_F(Vector3DTest, parallel) {
  ASSERT_TRUE(parallel(a, parallelToA));
  ASSERT_TRUE(parallel(a, -1 * parallelToA));
  ASSERT_FALSE(parallel(a, b));
}

TEST_F(Vector3DTest, dotProduct) {
  ASSERT_EQ(dotProduct(a,b), dotProductAB);
}

TEST_F(Vector3DTest, crossProduct) {
  ASSERT_EQ(crossProduct(a,b), crossProductAB);
}
