//
// Created by lennard on 7/6/20.
//

#include "TestFigure3D.h"

#include "algorithms/implementations/d3/OctreeTetraGridSearch.h"
#include "algorithms/implementations/d3/LinearTetraGridSearch.h"

class OctreeTest : public TestFigure3D {
protected:
  LinearTetraGridSearch lgs;
  OctreeTetraGridSearch ogs;
  Ray ray = {{0.4, 0.25, 1}, {1,1,-1}};

  void SetUp() override {
    TestFigure3D::SetUp();
    TetraGrid tg;
    loadTestData(tg);
    construct(ogs, tg, 4);
    construct(lgs, tg, 4);
    resetCounters();
  }
};

TEST_F(OctreeTest, getIntersectedBy) {
  auto result = getIntersectedBy(ogs, ray);
  ASSERT_EQ(RAY_INTERSECT_COUNT(AABox), 9);
  std::valarray<typename Region::size_type> expected = {4,2,4,2};
  ASSERT_TRUE((regionSizes(result) == expected).min());

  auto lgsResult = getIntersectedBy(lgs, ray);
  ASSERT_TRUE(isOrderedSubset(lgsResult, result));
}
