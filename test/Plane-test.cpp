//
// Created by lennard on 7/6/20.
//

#include "TestFigure3D.h"
#include "TestFigure2D.h"

class Plane3DTest : public TestFigure3D {
protected:

  Point planeSupport = {2,-1,2};
  Vector planeNormal = {1,1,1};
  Plane plane = {planeSupport, planeNormal};
  Point left = {0,1,0};
  Point right = {2,2,3};
  Point on = {3,0,0};
};

class Plane2DTest : public TestFigure2D {
protected:

  Point planeSupport = {2,1};
  Vector planeNormal = {1,1};
  Plane plane = {planeSupport, planeNormal};
  Point left = {0,1};
  Point right = {2,2};
  Point on = {3,0};
};

TEST_F(Plane3DTest, isLeft) {
  ASSERT_TRUE(isLeft(left, plane));
  ASSERT_FALSE(isLeft(right, plane));
  ASSERT_FALSE(isLeft(on, plane));
  ASSERT_FALSE(isLeft(planeSupport, plane));
}

TEST_F(Plane3DTest, isLeftOrOn) {
  ASSERT_TRUE(isLeftOrOn(left, plane));
  ASSERT_TRUE(isLeftOrOn(on, plane));
  ASSERT_TRUE(isLeftOrOn(planeSupport, plane));
  ASSERT_FALSE(isLeftOrOn(right, plane));
}

TEST_F(Plane2DTest, isLeft) {
  ASSERT_TRUE(isLeft(left, plane));
  ASSERT_FALSE(isLeft(right, plane));
  ASSERT_FALSE(isLeft(on, plane));
  ASSERT_FALSE(isLeft(planeSupport, plane));
}

TEST_F(Plane2DTest, isLeftOrOn) {
  ASSERT_TRUE(isLeftOrOn(left, plane));
  ASSERT_TRUE(isLeftOrOn(on, plane));
  ASSERT_TRUE(isLeftOrOn(planeSupport, plane));
  ASSERT_FALSE(isLeftOrOn(right, plane));
}