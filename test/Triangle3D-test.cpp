//
// Created by lennard on 7/6/20.
//

#include "TestFigure3D.h"

class Triangle3DTest : public TestFigure3D {
protected:

  Triangle triangle = {{0,1,0}, {1,1,1}, {0,0,1}};
  Ray intersectsTriangle = {{0,0.5,0.7}, {1,-0.2,-0.2}};
  Ray originOnEdge = {{0.5,1,0.5}, {0,-0.5,0}};
  Ray parallelToTriangle = {{0,0.5,0.5}, {1,0.5,0.5}};
  Ray notIntersecting = {{0.2,0,0}, {1,1,0.8}};
};

TEST_F(Triangle3DTest, intersectInclusiveRay) {
  ASSERT_TRUE(intersectInclusive(intersectsTriangle, triangle));
  ASSERT_TRUE(intersectInclusive(originOnEdge, triangle));
  ASSERT_FALSE(intersectInclusive(parallelToTriangle, triangle));
  ASSERT_FALSE(intersectInclusive(notIntersecting, triangle));
}

