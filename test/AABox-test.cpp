//
// Created by lennard on 7/6/20.
//

#include "TestFigure3D.h"

class AABox3DTest : public TestFigure3D {
protected:

  Point min = {1,1,1};
  Point max = {3,3,3};
  AABox aaBox = {min, max};
  Point pointInside = {2,2,2};
  Point pointOnLeftSide = {1,2,1};
  Point pointOnRightSide = {3,3,2};
  Point pointOnFrontSide = {2,1,3};
  Point pointOutside = {2,4,2};

  Ray intersectsFacets = {{0,1,1}, {1,1,1}};
  Ray intersectsOpposingCorners = {{0,0,0}, {1,1,1}};
  Ray intersectsLeftSideColinear = {{1,0,2}, {0,1,0}};
  Ray intersectsRightSideColinear = {{3,0,2}, {0,1,0}};
  Ray touchesCorner = {{0,0,0}, {1,3,1}};
  Ray touchesSide = {{0,0,2}, {1,3,0}};
  Ray startsOnSideNotCrossing = {{2, 3, 2}, {1, 1, 1}};
  Ray startsOnSideCrossing = {{2, 1, 2}, {1, 1, 1}};
  Ray notIntersecting = {{2, 1, 0}, {1, 1, 0.5}};
};

TEST_F(AABox3DTest, contains) {
  ASSERT_TRUE(contains(aaBox, pointInside));
  ASSERT_TRUE(contains(aaBox, pointOnLeftSide));
  ASSERT_TRUE(contains(aaBox, pointOnRightSide));
  ASSERT_TRUE(contains(aaBox, pointOnFrontSide));
  ASSERT_FALSE(contains(aaBox, pointOutside));
}

TEST_F(AABox3DTest, containsInInterior) {
  ASSERT_TRUE(containsInInterior(aaBox, pointInside));
  ASSERT_FALSE(containsInInterior(aaBox, pointOnLeftSide));
  ASSERT_FALSE(containsInInterior(aaBox, pointOnRightSide));
  ASSERT_FALSE(containsInInterior(aaBox, pointOnFrontSide));
  ASSERT_FALSE(containsInInterior(aaBox, pointOutside));
  ASSERT_FALSE(containsInInterior(aaBox, min));
  ASSERT_FALSE(containsInInterior(aaBox, max));
}

TEST_F(AABox3DTest, aabb) {

  auto boundingBox = aabb<AABox>( {pointInside, pointOnLeftSide, pointOnRightSide, pointOnFrontSide} );
  ASSERT_EQ(minPoint(boundingBox), min);
  ASSERT_EQ(maxPoint(boundingBox), max);
}

TEST_F(AABox3DTest, intersectRay) {

  ASSERT_TRUE(intersect(intersectsFacets, aaBox));
  ASSERT_TRUE(intersect(intersectsOpposingCorners, aaBox));
  ASSERT_TRUE(intersect(intersectsLeftSideColinear, aaBox));
  ASSERT_TRUE(intersect(intersectsRightSideColinear, aaBox));
  ASSERT_FALSE(intersect(touchesCorner, aaBox));
  ASSERT_FALSE(intersect(touchesSide, aaBox));
  ASSERT_FALSE(intersect(startsOnSideNotCrossing, aaBox));
  ASSERT_TRUE(intersect(startsOnSideCrossing, aaBox));
  ASSERT_FALSE(intersect(notIntersecting, aaBox));
}

//todo: wieder rein nehmen sobald AABox::intersectionPointsInclusive implementiert
//TEST_F(AABox3DTest, intersectInclusiveRay) {
//
//  ASSERT_TRUE(intersectInclusive(intersectsFacets, aaBox));
//  ASSERT_TRUE(intersectInclusive(intersectsOpposingCorners, aaBox));
//  ASSERT_TRUE(intersectInclusive(intersectsLeftSideColinear, aaBox));
//  ASSERT_TRUE(intersectInclusive(intersectsRightSideColinear, aaBox));
//  ASSERT_TRUE(intersectInclusive(touchesCorner, aaBox));
//  ASSERT_TRUE(intersectInclusive(touchesSide, aaBox));
//  ASSERT_TRUE(intersectInclusive(startsOnSideNotCrossing, aaBox));
//  ASSERT_TRUE(intersectInclusive(startsOnSideCrossing, aaBox));
//  ASSERT_FALSE(intersectInclusive(notIntersecting, aaBox));
//}

TEST_F(AABox3DTest, intersectionPointsInOut) {
  ASSERT_FALSE(intersectionPointsInOut(notIntersecting, aaBox));
  auto inOut = intersectionPointsInOut(intersectsFacets, aaBox);
  ASSERT_TRUE(inOut);
  Point expectedIn = {1,2,2};
  Point expectedOut = {2,3,3};
  ASSERT_EQ(inOut->first, expectedIn);
  ASSERT_EQ(inOut->second, expectedOut);
}

//todo: 2D intersectRay
